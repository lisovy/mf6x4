/*
 * UIO driver fo Humusoft MF614 DAQ card.
 * Copyright (C) 2010--2011 Rostislav Lisovy <lisovy@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/pci.h>
#include <linux/slab.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/uio_driver.h>

#define PCI_VENDOR_ID_HUMUSOFT		0x186c
#define PCI_DEVICE_ID_MF614		0x0614
#define PCI_SUBVENDOR_ID_HUMUSOFT	0x186c
#define PCI_SUBDEVICE_DEVICE		0x0000

#define STATUS_REG			0x10
#define STATUS_T5INTE			(u32) (1 << 19)
#define STATUS_T5			(u32) (1 << 3)
#define STATUS_CCINTE			(u32) (1 << 18)
#define STATUS_CC			(u32) (1 << 2)


static irqreturn_t mf614_irq_handler(int irq, struct uio_info *info)
{
	void __iomem *status_reg = info->priv;
	status_reg = (u8 *)status_reg + STATUS_REG;


	if ((ioread32(status_reg) & STATUS_T5INTE)
	&& (ioread32(status_reg) & STATUS_T5)) {
		/* disable interrupt */
		iowrite32(ioread32(status_reg) & ~STATUS_T5INTE, status_reg);
		return IRQ_HANDLED;
	}

	if ((ioread32(status_reg) & STATUS_CCINTE)
	&& (ioread32(status_reg) & STATUS_CC)) {
		/* disable interrupt */
		iowrite32(ioread32(status_reg) & ~STATUS_CCINTE, status_reg);
		return IRQ_HANDLED;
	}

	return IRQ_NONE;
}

static int mf614_irqcontrol(struct uio_info *info, s32 irq_on)
{
	void __iomem *status_reg = info->priv;
	status_reg = (u8 *)status_reg + STATUS_REG;


	if (irq_on == 0) { /* Disable interrupts */
		iowrite32(ioread32(status_reg) & ~STATUS_T5INTE, status_reg);
		iowrite32(ioread32(status_reg) & ~STATUS_CCINTE, status_reg);
	} else if (irq_on == 1) {
		iowrite32(ioread32(status_reg) | STATUS_T5INTE, status_reg);
		iowrite32(ioread32(status_reg) | STATUS_CCINTE, status_reg);
	}

	return 0;
}

static int __devinit mf614_pci_probe(struct pci_dev *dev,
				const struct pci_device_id *id)
{
	struct uio_info *info;

	info = kzalloc(sizeof(struct uio_info), GFP_KERNEL);
	if (!info)
		return -ENOMEM;

	if (pci_enable_device(dev))
		goto out_free;

	if (pci_request_regions(dev, "mf614"))
		goto out_disable;

	info->name = "MF614";
	info->version = "0.0.1";

	info->port[0].name = "Board programming registers";
	info->port[0].porttype = UIO_PORT_X86;
	info->port[0].start = pci_resource_start(dev, 0);
	if (!info->port[0].start)
		goto out_release;
	info->port[0].size = pci_resource_len(dev, 0);


	info->port[1].name = "OX9162 local configuration registers";
	info->port[1].porttype = UIO_PORT_X86;
	info->port[1].start = pci_resource_start(dev, 2);
	if (!info->port[1].start)
		goto out_release;
	info->port[1].size = pci_resource_len(dev, 2);

	info->priv = pci_iomap(dev, 2, 0);
	if (!info->priv)
		goto out_release;


	info->mem[0].name = "Board programming registers";
	info->mem[0].addr = pci_resource_start(dev, 4);
	if (!info->mem[0].addr)
		goto out_release;
	info->mem[0].size = pci_resource_len(dev, 4);
	info->mem[0].memtype = UIO_MEM_PHYS;
	info->mem[0].internal_addr = pci_ioremap_bar(dev, 4);
	if (!info->mem[0].internal_addr)
		goto out_release;


	info->mem[1].name = "OX9162 local configuration registers";
	info->mem[1].addr = pci_resource_start(dev, 3);
	if (!info->mem[1].addr)
		goto out_release;
	info->mem[1].size = pci_resource_len(dev, 3);
	info->mem[1].memtype = UIO_MEM_PHYS;
	info->mem[1].internal_addr = pci_ioremap_bar(dev, 3);
	if (!info->mem[1].internal_addr)
		goto out_release;


	info->irq = dev->irq;
	info->irq_flags = IRQF_SHARED;
	info->handler = mf614_irq_handler;

	info->irqcontrol = mf614_irqcontrol;

	if (uio_register_device(&dev->dev, info))
		goto out_unmap;

	pci_set_drvdata(dev, info);

	return 0;

out_unmap:
	iounmap(info->mem[0].internal_addr);
	iounmap(info->mem[1].internal_addr);
	pci_iounmap(dev, info->priv);
out_release:
	pci_release_regions(dev);
out_disable:
	pci_disable_device(dev);
out_free:
	kfree(info);
	return -ENODEV;
}

static void mf614_pci_remove(struct pci_dev *dev)
{
	struct uio_info *info = pci_get_drvdata(dev);
	void __iomem *status_reg = info->priv;
	status_reg = (u8 *)status_reg + STATUS_REG;

	/* Disable interrupts */
	iowrite32(ioread32(status_reg) & ~STATUS_T5INTE, status_reg);
	iowrite32(ioread32(status_reg) & ~STATUS_CCINTE, status_reg);

	uio_unregister_device(info);
	pci_release_regions(dev);
	pci_disable_device(dev);
	pci_set_drvdata(dev, NULL);
	pci_iounmap(dev, info->priv);
	iounmap(info->mem[0].internal_addr);
	iounmap(info->mem[1].internal_addr);

	kfree(info);
}

static struct pci_device_id mf614_pci_id[] __devinitdata = {
	{
		.vendor = PCI_VENDOR_ID_HUMUSOFT,
		.device = PCI_DEVICE_ID_MF614,
		.subvendor = PCI_SUBVENDOR_ID_HUMUSOFT,
		.subdevice = PCI_SUBDEVICE_DEVICE,
	},
	{ 0, }
};
MODULE_DEVICE_TABLE(pci, mf614_pci_id);

static struct pci_driver mf614_pci_driver = {
	.name = "mf614",
	.id_table = mf614_pci_id,
	.probe = mf614_pci_probe,
	.remove = mf614_pci_remove,
};

static int __init mf614_init_module(void)
{
	return pci_register_driver(&mf614_pci_driver);
}

static void __exit mf614_exit_module(void)
{
	pci_unregister_driver(&mf614_pci_driver);
}

module_init(mf614_init_module);
module_exit(mf614_exit_module);

MODULE_LICENSE("GPL v2");
MODULE_AUTHOR("Rostislav Lisovy <lisovy@gmail.com>");
