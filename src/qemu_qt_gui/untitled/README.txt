What is this?
=============
This is Qt GUI interface to be used with MF624 device implemented in Qemu.

How to compile
============== 
Run "qmake && make" or open project in Qt Creator and then build it.

How to use
==========
First run Qemu with MF624 device loaded. 
Then run Qt GUI. As command line argument write port which is Qemu MF624 listening to.