#ifndef SOCKET_THREAD_H
#define SOCKET_THREAD_H

#include <QtGui>

class socket_thread : public QThread
{
	Q_OBJECT
	int sockfd; //Socket to server

public:
	socket_thread(QObject *parent);
	void run(); // this is virtual method, we must implement it in our subclass of QThread
	void socket_send(const char* str, double val);
};

#endif // SOCKET_THREAD_H
