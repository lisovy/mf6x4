#-------------------------------------------------
#
# Project created by QtCreator 2011-04-07T12:06:20
#
#-------------------------------------------------

#LIBS     = -lsocket
QT       += core gui

TARGET = untitled
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    socket_thread.cpp

HEADERS  += mainwindow.h \
    socket_thread.h \
    mf624_io_status.h

FORMS    += mainwindow.ui
