#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "socket_thread.h"


namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	Ui::MainWindow *ui;

private slots:
	void on_checkBox_stateChanged(int arg1);
	void on_checkBox_2_stateChanged(int arg1);
	void on_checkBox_3_stateChanged(int arg1);
	void on_checkBox_4_stateChanged(int arg1);
	void on_checkBox_5_stateChanged(int arg1);
	void on_checkBox_6_stateChanged(int arg1);
	void on_checkBox_7_stateChanged(int arg1);
	void on_checkBox_8_stateChanged(int arg1);

	void on_adc0_valueChanged(double arg1);
	void on_adc1_valueChanged(double arg1);

	void on_adc2_valueChanged(double arg1);

	void on_adc3_valueChanged(double arg1);

	void on_adc4_valueChanged(double arg1);

	void on_adc5_valueChanged(double arg1);

	void on_adc6_valueChanged(double arg1);

	void on_adc7_valueChanged(double arg1);

private:
	int DIN; //DIN register

	void din_set(int bit, int val);
	socket_thread *s_thread;
};

#endif // MAINWINDOW_H
