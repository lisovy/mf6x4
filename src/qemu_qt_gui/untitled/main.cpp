#include <QtGui/QApplication>
#include "mainwindow.h"
#include "mf624_io_status.h"

mf624_io_status_t mf624_io_status;

int main(int argc, char *argv[])
{	
	if (argc < 2) {
		fprintf(stderr, "Usage %s PORT\n", argv[0]);
		::exit(1);
	}

	mf624_io_status.portno = atoi(argv[1]);
	mf624_io_status.DOUT = 0;
	mf624_io_status.DIN = 0;
	mf624_io_status.DA0 = 0;
	mf624_io_status.DA1 = 0;
	mf624_io_status.DA2 = 0;
	mf624_io_status.DA3 = 0;
	mf624_io_status.DA4 = 0;
	mf624_io_status.DA5 = 0;
	mf624_io_status.DA6 = 0;
	mf624_io_status.DA7 = 0;

	mf624_io_status.AD0 = 0;
	mf624_io_status.AD1 = 0;
	mf624_io_status.AD2 = 0;
	mf624_io_status.AD3 = 0;
	mf624_io_status.AD4 = 0;
	mf624_io_status.AD5 = 0;
	mf624_io_status.AD6 = 0;
	mf624_io_status.AD7 = 0;


	QApplication a(argc, argv);
	MainWindow w;
	w.show();

	return a.exec();
}
