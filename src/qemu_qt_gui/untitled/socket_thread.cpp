#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include "socket_thread.h"
#include "mf624_io_status.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
extern mf624_io_status_t mf624_io_status;


socket_thread::socket_thread(QObject *parent)
: QThread(parent)
{
	sockfd = -1;
}

void socket_thread::socket_send(const char* str, double val)
{
	int n;
	char buffer[256];
	sprintf(buffer, str, val);
	//printf(str, val);

	n = ::write(sockfd, buffer, strlen(buffer));
	if (n < 0) {
		perror("write()");
		//qDebug() << "MF624 in Qemu not runnig?";
	}
}


void socket_thread::run()
{
#define STRING_BUFF_SIZE	256
	//qDebug() << "Executing in new independant thread, GUI is NOT blocked";

	//int portno;
	int n;
	int status;
	struct sockaddr_in serv_addr;
	struct hostent *server;
	char read_buffer[STRING_BUFF_SIZE];
	char reg[STRING_BUFF_SIZE+1];
	float val;



	sockfd = ::socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		perror("socket()");
		::exit(0);
	}

	//server = gethostbyname(argv[1]);
	server = ::gethostbyname("127.0.0.1");
	if (server == NULL) {
		perror("gethostbyname()");
		::exit(0);
	}

	memset((char *) &serv_addr, '\0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr,
	     (char *)&serv_addr.sin_addr.s_addr,
	     server->h_length);

	serv_addr.sin_port = htons(mf624_io_status.portno);
	if (::connect(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
		perror("connect()");
		::exit(1);
	}

	// Client has succesfully connected to server (Qemu); Send status of all registers
	socket_send("DIN=%d\n", mf624_io_status.DIN);
	socket_send("ADC0=%f\n", ((MainWindow*) parent())->ui->adc0->value());
	socket_send("ADC1=%f\n", ((MainWindow*) parent())->ui->adc1->value());
	socket_send("ADC2=%f\n", ((MainWindow*) parent())->ui->adc2->value());
	socket_send("ADC3=%f\n", ((MainWindow*) parent())->ui->adc3->value());
	socket_send("ADC4=%f\n", ((MainWindow*) parent())->ui->adc4->value());
	socket_send("ADC5=%f\n", ((MainWindow*) parent())->ui->adc5->value());
	socket_send("ADC6=%f\n", ((MainWindow*) parent())->ui->adc6->value());
	socket_send("ADC7=%f\n", ((MainWindow*) parent())->ui->adc7->value());


	while(1) {
		memset(read_buffer, '\0', 256);
		n = read(sockfd, read_buffer, 255);
		if (n < 0) {
			perror("read()");
			::exit(2);
		}

		status = sscanf(read_buffer, "%[A-Z0-9]=%f", reg, &val);
		if (status == 2) {
			//printf("%s=%f\n", reg, val);
			if(!strcmp(reg, "DOUT")) {
				mf624_io_status.DOUT = val;
			}
			if(!strcmp(reg, "DA0")) {
				mf624_io_status.DA0 = val;
			}
			if(!strcmp(reg, "DA1")) {
				mf624_io_status.DA1 = val;
			}
			if(!strcmp(reg, "DA2")) {
				mf624_io_status.DA2 = val;
			}
			if(!strcmp(reg, "DA3")) {
				mf624_io_status.DA3 = val;
			}
			if(!strcmp(reg, "DA4")) {
				mf624_io_status.DA4 = val;
			}
			if(!strcmp(reg, "DA5")) {
				mf624_io_status.DA5 = val;
			}
			if(!strcmp(reg, "DA6")) {
				mf624_io_status.DA6 = val;
			}
			if(!strcmp(reg, "DA7")) {
				mf624_io_status.DA7 = val;
			}
		}

		//Setting DOUT
		((MainWindow*) parent())->ui->checkBox_9->setChecked(false);
		((MainWindow*) parent())->ui->checkBox_10->setChecked(false);
		((MainWindow*) parent())->ui->checkBox_11->setChecked(false);
		((MainWindow*) parent())->ui->checkBox_12->setChecked(false);
		((MainWindow*) parent())->ui->checkBox_13->setChecked(false);
		((MainWindow*) parent())->ui->checkBox_14->setChecked(false);
		((MainWindow*) parent())->ui->checkBox_15->setChecked(false);
		((MainWindow*) parent())->ui->checkBox_16->setChecked(false);
		if (mf624_io_status.DOUT & (1 << 0)) {
			((MainWindow*) parent())->ui->checkBox_9->setChecked(true);
		}
		if (mf624_io_status.DOUT & (1 << 1)) {
			((MainWindow*) parent())->ui->checkBox_10->setChecked(true);
		}
		if (mf624_io_status.DOUT & (1 << 2)) {
			((MainWindow*) parent())->ui->checkBox_11->setChecked(true);
		}
		if (mf624_io_status.DOUT & (1 << 3)) {
			((MainWindow*) parent())->ui->checkBox_12->setChecked(true);
		}
		if (mf624_io_status.DOUT & (1 << 4)) {
			((MainWindow*) parent())->ui->checkBox_13->setChecked(true);
		}
		if (mf624_io_status.DOUT & (1 << 5)) {
			((MainWindow*) parent())->ui->checkBox_14->setChecked(true);
		}
		if (mf624_io_status.DOUT & (1 << 6)) {
			((MainWindow*) parent())->ui->checkBox_15->setChecked(true);
		}
		if (mf624_io_status.DOUT & (1 << 7)) {
			((MainWindow*) parent())->ui->checkBox_16->setChecked(true);
		}

		// Setting DAC
		((MainWindow*) parent())->ui->dac0->setValue(mf624_io_status.DA0);
		((MainWindow*) parent())->ui->dac1->setValue(mf624_io_status.DA1);
		((MainWindow*) parent())->ui->dac2->setValue(mf624_io_status.DA2);
		((MainWindow*) parent())->ui->dac3->setValue(mf624_io_status.DA3);
		((MainWindow*) parent())->ui->dac4->setValue(mf624_io_status.DA4);
		((MainWindow*) parent())->ui->dac5->setValue(mf624_io_status.DA5);
		((MainWindow*) parent())->ui->dac6->setValue(mf624_io_status.DA6);
		((MainWindow*) parent())->ui->dac7->setValue(mf624_io_status.DA7);

		//printf("%s\n", read_buffer);
	}
	close(sockfd);

	exec();
}

