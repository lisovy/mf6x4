#ifndef MF624_IO_STATUS_H
#define MF624_IO_STATUS_H


typedef struct {
	int portno;
	unsigned int DOUT;
	unsigned int DIN;
	double DA0;
	double DA1;
	double DA2;
	double DA3;
	double DA4;
	double DA5;
	double DA6;
	double DA7;

	double AD0;
	double AD1;
	double AD2;
	double AD3;
	double AD4;
	double AD5;
	double AD6;
	double AD7;
} mf624_io_status_t;

#endif // MF624_IO_STATUS_H
