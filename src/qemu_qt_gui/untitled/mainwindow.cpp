#include <stdio.h>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "socket_thread.h"
#include "mf624_io_status.h"
extern mf624_io_status_t mf624_io_status;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	// Set fixed size for window
	this->setFixedSize(this->width(), this->height());

	// create new thread (on heap) and start it
	s_thread = new socket_thread(this);
	s_thread->start(); // after this, thread's run() method starts
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::din_set(int bit, int val)
{
	if(val == 0) {
		mf624_io_status.DIN &= ~(1 << bit);
	}
	else if (val == 2) {
		mf624_io_status.DIN |= (1 << bit);
	}

	s_thread->socket_send("DIN=%f\n", (double)(mf624_io_status.DIN));
}

void MainWindow::on_checkBox_stateChanged(int arg1)
{
	din_set(0, arg1);
}

void MainWindow::on_checkBox_2_stateChanged(int arg1)
{
	din_set(1, arg1);
}

void MainWindow::on_checkBox_3_stateChanged(int arg1)
{
	din_set(2, arg1);
}

void MainWindow::on_checkBox_4_stateChanged(int arg1)
{
	din_set(3, arg1);
}

void MainWindow::on_checkBox_5_stateChanged(int arg1)
{
	din_set(4, arg1);
}

void MainWindow::on_checkBox_6_stateChanged(int arg1)
{
	din_set(5, arg1);
}

void MainWindow::on_checkBox_7_stateChanged(int arg1)
{
	din_set(6, arg1);
}

void MainWindow::on_checkBox_8_stateChanged(int arg1)
{
	din_set(7, arg1);
}

void MainWindow::on_adc0_valueChanged(double arg1)
{
	s_thread->socket_send("ADC0=%f\n", arg1);
}

void MainWindow::on_adc1_valueChanged(double arg1)
{
	s_thread->socket_send("ADC1=%f\n", arg1);
}

void MainWindow::on_adc2_valueChanged(double arg1)
{
	s_thread->socket_send("ADC2=%f\n", arg1);
}

void MainWindow::on_adc3_valueChanged(double arg1)
{
	s_thread->socket_send("ADC3=%f\n", arg1);
}

void MainWindow::on_adc4_valueChanged(double arg1)
{
	s_thread->socket_send("ADC4=%f\n", arg1);
}

void MainWindow::on_adc5_valueChanged(double arg1)
{
	s_thread->socket_send("ADC5=%f\n", arg1);
}

void MainWindow::on_adc6_valueChanged(double arg1)
{
	s_thread->socket_send("ADC6=%f\n", arg1);
}

void MainWindow::on_adc7_valueChanged(double arg1)
{
	s_thread->socket_send("ADC7=%f\n", arg1);
}
