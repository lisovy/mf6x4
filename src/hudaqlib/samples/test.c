#include <stdio.h>
#include "hudaqlib.h"


int main(void)
{
	HUDAQHANDLE h;
	const HudaqResourceInfo *HRI;
	int i,j;
	double value;
	int NoAnalogIn,NoDigitalIn;

	//  h = HudaqOpenDevice("PCI1753",1,0);
	//  h = HudaqOpenDevice("PCD7004",1,0);
	//  h = HudaqOpenDevice("PCT7303B",1,0);
	h = HudaqOpenDevice("MF614", 1, 0);
	if (h == 0)
	{
		h = HudaqOpenDevice("MF624", 1, 0);
		//printf("h = %d\n", h);
		if (h == 0) {
			printf("No HUDAQ device found\n");
			return -1;
		}
	}  

	HRI = HudaqGetDeviceResources(h);
	for(i = 0; i < HRI->NumMemResources; i ++)
	{
		printf("\n Memory resource %d: Base: %Xh, Length: %Xh",
				i, HRI->MemResources[i].Base, HRI->MemResources[i].Length);
	}
	for(i = 0; i < HRI->NumIOResources; i ++)
	{
		printf("\n IO resource %d: Base: %Xh, Length: %Xh",
				i, HRI->IOResources[i].Base, HRI->IOResources[i].Length);
	}


	NoAnalogIn = HudaqGetParameter(h, 0, HudaqAINUMCHANNELS);
	printf("\nAnalog channels %d", NoAnalogIn);
	for (i = 0; i < NoAnalogIn; i ++)
	{
		value = HudaqAIRead(h, i);
		printf("\n Analog channel %d, value read %fV.", i, value);
	}

	NoDigitalIn = HudaqGetParameter(h, 0, HudaqDINUMCHANNELS);
	printf("\nDigital channels %d", NoDigitalIn);
	for (i = 0; i < NoDigitalIn; i++)
	{
		printf("\n Digital input %d: %d", i, HudaqDIRead(h, i));
	}

	HudaqCloseDevice(h);
	printf("\n");
	return 0;
}
