/******************************************************************/
/**@file opendev.c:
 * Desctiption: Humusoft data acquisition Linux library
 implementation.                                   *
 * Dependency: Linux                                              *
 *                Copyright 2006-2007 Jaroslav Fojtik             *
 ******************************************************************/
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <dirent.h>

#include "hudaqlib.h"
#include "hudaq_internal.h"


static int sysfs_get_value(const char *FileName)
{
	FILE *F;
	int RetVal;

	F = fopen(FileName, "rb");
	if(F == NULL)
		return -1;

	fscanf(F, "%X", &RetVal);
	fclose(F);
	return RetVal;
}

static int ScanSys(DWORD DeviceID, int deviceorder, DriverRecord *Rec)
{
	DIR *dir_uio, *dir_memmaps;
	FILE *F;
	char ch;
	struct dirent *direntry_uio, *direntry_memmaps;
	unsigned vendor_id;
	unsigned device_id;
	char FileName[1024];
	const char *SysDir="/sys/class/uio/";
	unsigned long long start, end, size;
	unsigned int index;
	DWORD Resources[7 * 3];
	int i;
	int j = 0;
	int RetVal = 0;

	if(deviceorder <= 0)
		return -1;
	if(Rec == NULL)
		return -2;

	Rec->NumPhysMemResources = Rec->Resources.NumIOResources = 0;

	sprintf(FileName, "%s", SysDir);
	dir_uio = opendir(FileName);
	if(!dir_uio)
	{
		perror("opendir()");
		return -3;
	}

	while (direntry_uio = readdir(dir_uio))
	{
		if((strcmp((direntry_uio->d_name), ".") == 0) || 
			(strcmp((direntry_uio->d_name), "..") == 0))
		{
			continue;
		}

		sprintf(FileName, "%s/%s/device/vendor", SysDir, direntry_uio->d_name);
		vendor_id = sysfs_get_value(FileName);
		sprintf(FileName, "%s/%s/device/device", SysDir, direntry_uio->d_name);
		device_id = sysfs_get_value(FileName);


		//Fixme search also through iomaps; add into the same Resources array	
		memset(Resources, 0, sizeof(Resources)); 
		sprintf(FileName, "%s/%s/maps", SysDir, direntry_uio->d_name);
		dir_memmaps = opendir(FileName);
		if (!dir_memmaps) {
			perror("opendir()");
			printf("%s\n", FileName);
			return -3;
		}
		while (direntry_memmaps = readdir(dir_memmaps)) {
			if((strcmp(direntry_memmaps->d_name, ".") == 0) || (strcmp(direntry_memmaps->d_name, "..") == 0))
				continue;

			//printf("maps: %s\n", direntry_memmaps->d_name);
			sprintf(FileName, "%s/%s/maps/%s/addr", SysDir, direntry_uio->d_name, direntry_memmaps->d_name);
			F = fopen(FileName, "rb");
			if(F)
			{
				if(fscanf(F, "%llx", &start) != 1)
				{
					fprintf(stderr, "Syntax error in %s", FileName);
					break;
				}

				Resources[j] = start;
				fclose(F);
			}

			sprintf(FileName, "%s/%s/maps/%s/size", SysDir, direntry_uio->d_name, direntry_memmaps->d_name);
			F = fopen(FileName, "rb");
			if(F)
			{
				if(fscanf(F, "%llx", &size) != 1)
				{
					fprintf(stderr, "Syntax error in %s", FileName);
					break;
				}

				Resources[j + 7] = size;
		
				fclose(F);
			}
				

			index = atoi((direntry_memmaps->d_name) + 3); // "+3", because we have to skip over the word "map"
			Resources[j + 7 + 7] = index;
			j ++;
		}
		
		closedir(dir_memmaps);    

		if(DeviceID == (device_id + (vendor_id << 16)))
		{
			//printf("DEVICE HAS BEEN FOUND!\n");
			if(--deviceorder < 1)
			{
				for(i = 0; i < 7; i++)
				{
					if(Resources[i] != 0)
					{
						if(Resources[i] > 0xFFFF)
						{
							Rec->PhysMemResources[Rec->NumPhysMemResources].Base = Resources[i];
							Rec->PhysMemResources[Rec->NumPhysMemResources].Length = Resources[i + 7];
							Rec->PhysMemResources[Rec->NumPhysMemResources].UioMappingIndex	= 
								Resources[i + 7 + 7];
							Rec->NumPhysMemResources ++;
						}
						else
						{
							Rec->Resources.IOResources[Rec->Resources.NumIOResources].Base = Resources[i];
							Rec->Resources.IOResources[Rec->Resources.NumIOResources].Length = Resources[i + 7];
							Rec->Resources.IOResources[Rec->Resources.NumIOResources].UioMappingIndex =
								 Resources[i + 7 + 7];
							Rec->Resources.NumIOResources ++;
						}
					}
				}

				Rec->UioDevNum = atoi((direntry_uio->d_name) + 3); //number of uio device

				RetVal = 1;    //device has been found!!
				break;
			}
		}    

		//printf("%X %X %s\n", vendor_id, device_id, direntry_uio->d_name);
	}

	closedir(dir_uio);
	return RetVal;
}






/*****************************************************************************
  ;*
  ;*              OpenDeviceHandle
  ;*              opens device handle by device name and order
  ;*
  ;*              Input:  device name
  ;*                      device order number
  ;*              Output: positive number when success; 0 or negative when fails.
  ;*
  ;****************************************************************************/
int OpenDeviceHandle(const char *devicename, int deviceorder, DriverRecord *Rec)
{
	DWORD DeviceID = 0;
	int RetVal;

	if(deviceorder <= 0)
		return -1;
	if(Rec == NULL)
		return -2;

	/* Humusoft */
#ifdef MF61X
	if(!strcmp(devicename,"AD612")) DeviceID = 0x186C0612; /*DeviceID = (Device_ID & (Vendor_ID << 16))*/
	if(!strcmp(devicename,"MF614")) DeviceID = 0x186C0614;
#endif
#ifdef MF62X
	if(!strcmp(devicename,"AD622")) DeviceID = 0x186C0622;
	if(!strcmp(devicename,"MF624")) DeviceID = 0x186C0624;
	if(!strcmp(devicename,"MF625")) DeviceID = 0x186C0625;
#endif  

	/* Advantech */
#ifdef PCI1753
	if(!strcmp(devicename,"PCI1753")) DeviceID = 0x13FE1753;
#endif

	/* Tedia */  
#ifdef PCD7004
	if(!strcmp(devicename,"PCD7004")) DeviceID = 0x17600101;
#endif
#ifdef PCT7303B
	if(!strcmp(devicename,"PCT7303B")) DeviceID = 0x17600201;
#endif

	//printf("\n%s deviceID = %X", devicename, DeviceID);

	if(DeviceID == 0)
		return -3;

	RetVal = ScanSys(DeviceID, deviceorder, Rec);
	return RetVal;
}


