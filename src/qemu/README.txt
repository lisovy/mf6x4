1 -- How to compile Qemu
========================
* Download latest Qemu release from http://wiki.qemu.org/Download 
  (e. g. http://download.savannah.gnu.org/releases/qemu/qemu-0.14.0.tar.gz)

* Untar.
* Run 
  $ ./configure --enable-system  --target-list=i386-softmmu 

  If you want SDL console (instead of VNC connection) run
  $ ./configure --enable-system  --target-list=i386-softmmu --enable-sdl

* Than run
  $ make

If you don't want to install Qemu, your binary is in "i386-softmmu" directory 


2 -- How to compile Qemu with MF624
===================================
* Compile Qemu as described in paragraph 1.
* Copy ./hw/mf624.c into /hw folder of your Qemu building directory.
* Add line "hw-obj-$(CONFIG_PCI) += mf624.o" into /Makefile.objs (should be in 
  your Qemu building directory) about before "hw-obj-$(CONFIG_NE2000_PCI) += ne2000.o"
  line.
* Run
  $ make


3 -- How to run Qemu with MF624
===============================
* Add "-device mf624" to Qemu command line parameters.
  E. g. "i386-softmmu$ ./qemu  -device mf624 -hda ..//debian.qcow --boot c"

* Virtual MF624 card communicates with GUI interface through TCP/IP protocol. 
  For choosing particular port number use "-device mf624,port=55555", 
  where 55555 is your port number.
