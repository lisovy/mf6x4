#ifndef _mf624_h_
#define _mf624_h_


/* Subdevice numbers */
#define MF624_SUBDEV_COUNT	4 /* Nr of subdevices */
#define MF624_DO_SUBDEV		3
#define MF624_DI_SUBDEV		2
#define MF624_AO_SUBDEV		1
#define MF624_AI_SUBDEV		0

#endif /* _mf624_h_ */