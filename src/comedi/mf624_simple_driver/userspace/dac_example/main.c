#include <stdio.h>
#include <comedilib.h>
#include "../mf624.h"

/* Converts Volts into format needed by card.
Input should be integer (no float) */
int phys_to_comedi(int val)
{
	if (val >= -10 && val <= 10) {
		return 0x2000 + (val * (0x2000/10));
	}
	else {
		return 0x2000; // 0V
	}
}

int main(int argc, char* argv[])
{
	comedi_t* comedi_dev;
	int status;
	int channel = 0;
	int range = 0;
	int aref = 0;

	comedi_dev = comedi_open("/dev/comedi0");
	if (comedi_dev == NULL) {
		comedi_perror("comedi_open");
		return 1;
	}

	while(1) {
		printf("Setting DA0 to 5 V \n");
		status = comedi_data_write(comedi_dev, MF624_AO_SUBDEV, channel, range, aref, phys_to_comedi(5));
		if (status == -1) {
			comedi_perror("comedi_data_write");
		}
		sleep(1);
	}

	return 0;
}
