#include <stdio.h>
#include <comedilib.h>
#define MF614_DO_SUBDEV		1

int main(int argc, char* argv[])
{
	comedi_t* comedi_dev;
	int status;

	comedi_dev = comedi_open("/dev/comedi0");
	if (comedi_dev == NULL) {
		comedi_perror("comedi_open");
		return 1;
	}

	while(1) {
		printf("Setting DO to 0b00000001\n");
		status = comedi_dio_write(comedi_dev, MF614_DO_SUBDEV, 0, 1);
		status = comedi_dio_write(comedi_dev, MF614_DO_SUBDEV, 1, 0);
		if (status == -1) {
			comedi_perror("comedi_dio_write");
		}
		sleep(1);

		printf("Setting DO to 0b00000010\n");
		status = comedi_dio_write(comedi_dev, MF614_DO_SUBDEV, 0, 0);
		status = comedi_dio_write(comedi_dev, MF614_DO_SUBDEV, 1, 1);
		if (status == -1) {
			comedi_perror("comedi_dio_write");
		}
		sleep(1);
	}

	return 0;
}
