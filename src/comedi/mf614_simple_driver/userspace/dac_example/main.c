#include <stdio.h>
#include <comedilib.h>
#define MF614_AO_SUBDEV	3



int main(int argc, char* argv[])
{
	comedi_t* comedi_dev;
	int status;
	int channel = 0;
	int range = 0;
	int aref = 0;

	comedi_dev = comedi_open("/dev/comedi0");
	if (comedi_dev == NULL) {
		comedi_perror("comedi_open");
		return 1;
	}

	while(1) {
		printf("Setting DA0 to 9.9 V \n");
		status = comedi_data_write(comedi_dev, MF614_AO_SUBDEV, channel, range, aref, 0xFFF); // 9.9 V
		if (status == -1) {
			comedi_perror("comedi_data_write");
		}
		sleep(1);
		
		printf("Setting DA0 to 0 V\n");
		status = comedi_data_write(comedi_dev, MF614_AO_SUBDEV, channel, range, aref, 0x800); // 0 V
		if (status == -1) {
			comedi_perror("comedi_data_write");
		}
		sleep(1);
	}

	return 0;
}
