How to compile MF614 & MF624 Comedi driver
==========================================
* Copy driver source into kernel tree
  # cp mf624.c /usr/src/linux-source-2.6.32/drivers/staging/comedi/drivers

* Tell Comedi to compile it
  # cd /usr/src/linux-source-2.6.32/
  # echo "obj-\$(CONFIG_COMEDI_PCI_DRIVERS) += mf624.o" >> drivers/staging/comedi/drivers/Makefile 

* Compile it
  # make drivers/staging/comedi/ O=/usr/src/linux-source-2.6.32/_build/686
  # make M=drivers/staging/comedi/ O=/usr/src/linux-source-2.6.32/_build/686

How to load legacy (!) drivers
==============================
* Load Comedi
  # modprobe comedi comedi_num_legacy_minors=4

* Load MF624 Driver
  # cd _build/686/drivers/staging/comedi/drivers
  # insmod ./mf624.ko
  # dmesg | tail -3
   [ 6184.326066] comedi: module is from the staging directory, the quality is unknown, you have been warned.
   [ 6184.331917] comedi: version 0.7.76 - http://www.comedi.org
   [ 6309.161789] mf624: module is from the staging directory, the quality is unknown, you have been warned.

* Configure loaded driver (!)
  # comedi_config /dev/comedi0 mf624
  # dmesg | tail -11
   [ 6374.717904] comedi0: mf624: driver: Bourgeot - Poulain 2006-2007
   [ 6374.717909] This is an experimental version, you can report          some remarks or problems to fpoulain@gmail.com
   [ 6374.717929] comedi0: mf624: Found mf624 on bus 1, slot 11
   [ 6374.717950] pci 0000:01:0b.0: PCI INT A -> GSI 22 (level, low) -> IRQ 22
   [ 6374.717983] comedi0: mf624: PCI Resource 0    addr d2dffc00 
   [ 6374.717987] comedi0: mf624: PCI Resource 1    addr d2dff800 
   [ 6374.717989] comedi0: mf624: PCI Resource 2    addr d2dff400 
   [ 6374.717993] comedi0: mf624: IO_BADR0    addr e081cc00 
   [ 6374.717996] comedi0: mf624: IO_BADR1(2) addr e0846800 
   [ 6374.717999] comedi0: mf624: IO_BADR2(4) addr e084e400 
   [ 6374.718002] comedi0: mf624: Driver attached

How to load modern drivers
==========================
* Load Comedi
  # modprobe comedi

* Load MF614 Driver
  # cd _build/686/drivers/staging/comedi/drivers
  # insmod ./mf614.ko
  # dmesg | tail -7
   [10543.431017] mf614 0000:01:0a.0: PCI INT A disabled
   [10566.126096] comedi: module is from the staging directory, the quality is unknown, you have been warned.
   [10566.132780] comedi: version 0.7.76 - http://www.comedi.org
   [10585.089172] mf614: module is from the staging directory, the quality is unknown, you have been warned.
   [10585.091866] comedi0: mf614: Found mf614 on bus 1, slot 10
   [10585.091903] mf614 0000:01:0a.0: PCI INT A -> GSI 21 (level, low) -> IRQ 21
   [10585.091915] comedi0: mf614: Driver attached

* No configuration is needed. /dev/comedi0 is ready to use
  # ls /dev/comedi*
   /dev/comedi0



