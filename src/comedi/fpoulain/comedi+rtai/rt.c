/***********************************************************************
Copyright (C) 2007  Francois Poulain, <fpoulain AT gmail DOT com>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************/
#include <stdio.h>
#include <time.h>
#include <sys/stat.h>
#include <sched.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <rtai_lxrt.h>
#include <rtai_sched.h>
#include <pthread.h>
#include <signal.h>
#include  <math.h>
#include <stdio.h>
#include <comedilib.h>

/* Some global variables */
static pthread_t thread_ptr;

int priority=20;   		/* Highest 		*/
int stack_size=0; 		/* Use default (512) 	*/
int msg_size=0;   		/* Use default (256) 	*/

#define TMAX 		10.0 			/* in seconds 		*/
#define FREQ		100.0			/* in seconds^-1 	*/
#define SAMPLE_PERIOD	1.0e09/50.0/FREQ	/* in nano seconds 	*/

float function(int toc){
	float time=toc*SAMPLE_PERIOD/1e09;
	return 0x2000 + 4000*sin(time*FREQ*2*M_PI);

}

static void* task(void *unused)
{
	RT_TASK *handler;
	comedi_t *device;
	lsampl_t data=0x1000;
	int ret;
	int subdev = 1;         /* change this to your input subdevice */
	int chan = 0;           /* change this to your channel */
	int range = 0;          /* more on this later */
	int aref = 0;           /* more on this later */
	char filename[] = "/dev/comedi0";
	int toc=0;
	long before=0, after=0;

	printf("# Required : %f nanoseconds, error in ns :\n", SAMPLE_PERIOD);

	/* Open comedi device */
	device = comedi_open(filename);
	if(!device){comedi_perror(filename);}

	/* Init RT Task */
	if(!(handler = rt_task_init( nam2num("rtTask"), priority, stack_size, msg_size))){
		printf("CANNOT INIT RT TASK\n"); 
	}

	/* Make thread periodic */
	rt_task_make_periodic (handler, rt_get_time()+nano2count(SAMPLE_PERIOD), nano2count(SAMPLE_PERIOD));
	
	/* By now we can do serious things */
	rt_make_hard_real_time();
	
	while(toc*SAMPLE_PERIOD/1e09 < TMAX)
	{
		/* wait the timer period */
		rt_task_wait_period();
		if(toc%5000==0){before = rt_get_time_ns();}
		if(toc%5000==1){
			after = rt_get_time_ns();
			if (after>before) {printf("%f\n", SAMPLE_PERIOD - after + before);}
		}

		/* Command computation */
		data = function(toc);
		
		/* Sending it to board */
		ret = comedi_data_write(device, subdev, chan, range, aref, data);
		if(ret < 0){comedi_perror(filename);}

		/* Post iteration */
		toc++;
	}

	/* Clean up ... */	
	rt_make_soft_real_time();
	rt_task_delete(handler);

	return unused;
}

int main (void)
{
	struct sched_param mysched;

	/* Verification */
	if(getuid() != 0){fprintf(stderr, "must be root !\n"); exit(1);}
	

	/* Avoid swapping */
	mlockall(MCL_CURRENT | MCL_FUTURE);		

	/* Config scheduler */
	mysched.sched_priority = sched_get_priority_max(SCHED_FIFO) - 1;
	if( sched_setscheduler( 0, SCHED_FIFO, &mysched ) == -1 ){
		printf("ERROR IN SETTING THE SCHEDULER\n");
		perror("errno");
		exit(1);
	}

	/* Set and start timer */
	rt_set_periodic_mode();
	start_rt_timer(0);
	
	/* Launch main loop ... */
	if(pthread_create(&thread_ptr, NULL, task, NULL) != 0) {perror("Creation du thread");}
	/* ... and wait for it's end */
	pthread_join(thread_ptr, NULL);

	/* Terminus */
	stop_rt_timer();
	return(0);
}

