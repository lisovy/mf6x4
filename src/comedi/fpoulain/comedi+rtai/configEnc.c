/***********************************************************************
Copyright (C) 2007  Francois Poulain, <fpoulain AT gmail DOT com>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************/

#include <stdio.h>
#include <time.h>
#include <comedilib.h>
#include "mf624.h"

#define PERIOD 	250000000.0	/* in nanoseconds 	*/
#define TIME	2.0		/* in seconds  		*/

int main(void)
{
	comedi_t *device;
 	comedi_insn instruction;
	lsampl_t data[MF624_ENC_DATA_SIZE], dataReaded;
	int subdev = MF624_ENC_SUBDEV;
	int chan = 2;
	int range = 0;
	int aref = 0;
	char filename[] = "/dev/comedi0";
	
	int toc = 0;
	struct timespec tempo;

	tempo.tv_sec = PERIOD/1e9;
	tempo.tv_nsec = PERIOD - tempo.tv_sec*1e9;

	data[0] = MF624_ENC_MODE_RISING; 		/* Encoder mode 	*/
	data[1] = MF624_ENC_COUNT_ENABLE; 		/* Encoder counting 	*/
	data[2] = MF624_ENC_RESET_ENABLE_BYINPUT; 	/* Encoder reset 	*/
	data[3] = MF624_ENC_FILTER_ON;			/* Encoder filtering 	*/

	instruction.insn = INSN_CONFIG;
	instruction.n = MF624_ENC_DATA_SIZE;
	instruction.data = data;
	instruction.subdev = subdev;
	instruction.chanspec=CR_PACK(chan,range,aref);

//	comedi_loglevel(4);

	/* Open comedi device */
	device = comedi_open(filename);
	if(!device){
		comedi_perror(filename);
		return(1);
	}

	comedi_do_insn(device , &instruction);

	while(toc++ < TIME/(PERIOD/1000000000)){
		comedi_data_read(device, subdev, chan, range, aref, &dataReaded);
		printf("retour : %x\n", dataReaded);
		nanosleep(&tempo, NULL);
	}

	comedi_close(device);

	return 0;
}

