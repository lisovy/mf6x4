/***********************************************************************
Copyright (C) 2007  Francois Poulain, <fpoulain AT gmail DOT com>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************/

#include <stdio.h>
#include <comedilib.h>
#include <time.h>
#include <math.h>

#define TMAX 		10.0	 		/* in seconds 		*/
#define FREQ		1.0			/* in seconds^-1 	*/
#define SAMPLE_PERIOD	1.0e09/50.0/FREQ	/* in nano seconds 	*/

float function(int toc){
	float time=toc*SAMPLE_PERIOD/1e09;
	return 0x2000 + 4000*sin(time*FREQ*2*M_PI);
}

int main()
{
	comedi_t *device;
	lsampl_t data=0x1000;
	int ret;
	int subdev = 1;         /* change this to your input subdevice */
	int chan = 0;           /* change this to your channel */
	int range = 0;          /* more on this later */
	int aref = 0;           /* more on this later */
	char filename[] = "/dev/comedi0";
	int toc=0;

	/* Structure for nanosleep() argument, see 'man nanosleep' for more details */
	struct timespec tempo;
	struct timeval before, after;

	tempo.tv_sec=0;
	tempo.tv_nsec=SAMPLE_PERIOD;

	/* Open comedi device */
	device = comedi_open(filename);
	if(!device){
		comedi_perror(filename);
		return(1);
	}

	printf("# Required : %f microseconds, error in us :\n", SAMPLE_PERIOD/1000);

	while(toc*SAMPLE_PERIOD/1e09 < TMAX)
	{
		if(toc%50==10){gettimeofday(&before, NULL);}
		if(toc%50==11){
			gettimeofday(&after, NULL);;
			if (after.tv_usec > before.tv_usec) {printf("%f\n", SAMPLE_PERIOD/1000 - after.tv_usec + before.tv_usec);}
		}

		/* command computation */
		data = function(toc);

		/* sending to board */
		ret = comedi_data_write(device, subdev, chan, range, aref, data);
		if(ret < 0){
			comedi_perror(filename);
			return(1);
		}

		/* beautiful tempo ... */
		nanosleep(&tempo, NULL);

		/* post iteration */
		toc++;
	}

	return 0;
}

