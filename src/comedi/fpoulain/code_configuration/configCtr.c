/***********************************************************************
Copyright (C) 2007  Francois Poulain, <fpoulain AT gmail DOT com>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************/

#include <stdio.h>
#include <time.h>
#include <math.h>
#include <comedilib.h>
#include "mf624.h"

void mf624_timer_PWM_config(comedi_t *device, int channel, float freq, float ratio){
	comedi_insn instruction;
	unsigned int clk_source = 0;
	unsigned int registerA, registerB;
	float period = 1/freq, clk_freq = 0;
	lsampl_t data[13];
	lsampl_t registers[2];

	if(ratio < 0 || ratio > 1) {printf("bad ratio: %f\n", ratio); return;}

	if(freq > 50000000 || freq < 100000/powl(2,32))	{printf("Can't configure timer for such a frequency !\n"); return;}

	else if(freq > 100000){
		clk_source = MF624_CTR_CKSOURCE_50M;
		clk_freq = 50000000;
	}
	else if(freq > 10000){
		clk_source = MF624_CTR_CKSOURCE_10M;
		clk_freq = 10000000;
	}
	else if(freq > 1000){
		clk_source = MF624_CTR_CKSOURCE_1M;
		clk_freq = 1000000;
	}
	else{
		clk_source = MF624_CTR_CKSOURCE_100K;
		clk_freq = 100000;
	}

	registerA = -1-clk_freq*period*ratio;
	registerB = -1-clk_freq*period*(1-ratio);
	printf("freq = %f, clk_freq*period = %f, -A register = %u, -B register = %u\n", freq, clk_freq*period, -registerA, -registerB);

	registers[0] = registerA;
	registers[1] = registerB;

	instruction.insn = INSN_WRITE;
	instruction.subdev = MF624_CTR_SUBDEV;
	instruction.chanspec = CR_PACK(channel, 0, 0);
	instruction.n = 2;
	instruction.data = registers;

	comedi_do_insn(device , &instruction);

	data[0] = MF624_CTR_DIRECTION_UP;
	data[1] = MF624_CTR_REPETITION_ON;
	data[2] = MF624_CTR_PRELOAD_AB;
	data[3] = MF624_CTR_OUTPUT_BISTABLE;
	data[4] = MF624_CTR_TRIGGER_DISABLED;
	data[5] = MF624_CTR_TRIGGER_DISABLE;
	data[6] = MF624_CTR_RETRIGGER_OFF;
	data[7] = MF624_CTR_GATE_HIGHT;
	data[8] = MF624_CTR_GATEPOLARITY_LOW;
	data[9] = clk_source;
	data[10] = MF624_CTR_OUTPUT_DIRECT;
	data[11] = MF624_CTR_CTRL_LOAD;

	instruction.insn = INSN_CONFIG;
	instruction.n = 12;
	instruction.data = data;
	comedi_do_insn(device , &instruction);

	data[0] = MF624_CTR_CTRL_START;
	instruction.n = 1;
	
	comedi_do_insn(device , &instruction);
}

void mf624_timer_freq_config(comedi_t *device, int channel, float freq){
	mf624_timer_PWM_config(device, channel, freq, 0.5);
	return;
}

void mf624_timer_reset(comedi_t *device, int channel){
	comedi_insn instruction;
	lsampl_t data = MF624_CTR_CTRL_RESET;

	instruction.insn = INSN_CONFIG;
	instruction.n = 1;
	instruction.data = &data;
	instruction.subdev = MF624_CTR_SUBDEV;
	instruction.chanspec = CR_PACK(channel, 0, 0);;

	comedi_do_insn(device , &instruction);
}

int main(void)
{
	comedi_t *device;
	unsigned int chan = 0;
	float freq = 100.0, ratio = 0.0; /* in Hz */
	char filename[] = "/dev/comedi0";
	int dt = 0;
	struct timespec ts={0,20000000};

	/* Open comedi device */
	device = comedi_open(filename);
	if(!device){
		comedi_perror(filename);
		return(1);
	}

	mf624_timer_reset(device, chan);
	mf624_timer_freq_config(device, chan, freq);

	while (1){
		ratio = ratio + 1.0/20;
		if (ratio >= 1.0 || ratio < 0) ratio = 0;
		mf624_timer_PWM_config(device, chan, freq, ratio);
		nanosleep(&ts, NULL);
	}

	return 0;
}

