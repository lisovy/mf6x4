/***********************************************************************
Copyright (C) 2007  Francois Poulain, <fpoulain AT gmail DOT com>
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
***********************************************************************/

#include <stdio.h>
#include <time.h>
#include <comedilib.h>
#include "mf624.h"

#define PERIOD 	025000000.0	/* in nanoseconds 	*/
#define TIME	0.0		/* in seconds  		*/
#define INFINITE 0		/* don't stop		*/

#define PLOT_LEN 512		/* length of plot data	*/

lsampl_t tab[PLOT_LEN];

void plotDatas(lsampl_t* tableau){
	int i = 0;
	printf("plot [0:512][-512:512] '-' using 1:2 with lines\n");
	for(i = 0 ; i < PLOT_LEN ; i++)	printf("%d %d \n", i, tableau[i]);
	printf("e \n");
}

void mf624_enc_std_config(comedi_t *device, int subdev, int chanspec){
	comedi_insn instruction;
	lsampl_t data[4];
	data[0] = MF624_ENC_MODE_RISING;
	data[1] = MF624_ENC_COUNT_ENABLE;
	data[2] = MF624_ENC_RESET_ONRAISING_INPUT;
//	data[2] = MF624_ENC_RESET_DISABLE;
	data[3] = MF624_ENC_FILTER_ON;

	instruction.insn = INSN_CONFIG;
	instruction.n = 4;
	instruction.data = data;
	instruction.subdev = subdev;
	instruction.chanspec = chanspec;

	comedi_do_insn(device , &instruction);
}

void mf624_enc_reset(comedi_t *device, int subdev, int chanspec){
	comedi_insn instruction;
	lsampl_t data = MF624_ENC_RESET_ENABLE;

	instruction.insn = INSN_CONFIG;
	instruction.n = 1;
	instruction.data = &data;
	instruction.subdev = subdev;
	instruction.chanspec = chanspec;

	comedi_do_insn(device , &instruction);

	data = MF624_ENC_RESET_DISABLE;
	instruction.data = &data;
	comedi_do_insn(device , &instruction);
}

int main(void)
{
	comedi_t *device;
	lsampl_t dataReaded;
	int subdev = MF624_ENC_SUBDEV;
	int chan = 1;
	int range = 0;
	int aref = 0;
	char filename[] = "/dev/comedi0";

	int toc = 0, tic = 0;
	struct timespec tempo;
	
	printf("# You should pipe this program with gnuplot \n");

	// init tab
	for (tic = 0 ; tic < PLOT_LEN ; tic ++) tab[tic] = 0;

	tempo.tv_sec = PERIOD/1e9;
	tempo.tv_nsec = PERIOD - tempo.tv_sec*1e9;

//	comedi_loglevel(4);

	/* Open comedi device */
	device = comedi_open(filename);
	if(!device){
		comedi_perror(filename);
		return(1);
	}

//	mf624_enc_reset(device, subdev, CR_PACK(chan,range,aref));
	mf624_enc_std_config(device, subdev, CR_PACK(chan,range,aref));
	mf624_enc_std_config(device, subdev, CR_PACK(chan+1,range,aref));

	// read configuration register using virtual channel
	// comedi_data_read(device, subdev, 4, range, aref, &dataReaded);
	// printf("registre de configuration : %x\n", dataReaded);

	while(toc++ < TIME/(PERIOD/1000000000) || INFINITE){
		comedi_data_read(device, subdev, chan, range, aref, &dataReaded);
		tab[tic] = dataReaded;
		nanosleep(&tempo, NULL);
		tic++;
		if (tic >= 512) {
			tic = 0;
		}
		if (tic%8 == 0) plotDatas(tab);
	}

	comedi_close(device);

	return 0;
}

