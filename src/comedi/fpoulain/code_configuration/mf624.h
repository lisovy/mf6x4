/*
 * comedi/drivers/mf624.c
 * Code for a Humusoft MF624 driver
 *
 * COMEDI - Linux Control and Measurement Device Interface
 * Copyright (C) 2000 David A. Schleef <ds@schleef.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/*
Driver: mf624.o
Description: A comedi driver for Humusoft MF624 PCI Card
Devices: Humusoft MF624 Multifunction I/O Card
Author: Jean-Matthieu Bourgeot - François Poulain
Updated: Wed, 19 Sep 2007 14:29:43 +0200
Status: experimental

This driver is a driver for the Humusoft MF624 PCI Card.

It has :
 * 8 channel 14 bits ADC,
 * 8 channel 14 bits DAC,
 * 8 digital inputs,
 * 8 digital outputs,
 * 4 quadrature encoder input,
 * 5 timers/counter.

Status:
A/D Converter			: Supported on subdevice 0,
D/A Converter			: Supported on subdevice 1,
Digital Inputs			: Supported on subdevice 2,
Digital Outputs			: Supported on subdevice 3,
Quadrature Encoder Inputs	: Not yet supported,
Counters/Timers			: Not yet supported,
IRQ				: Not yet supported.

Remarks:
Simultaneous D/A update not yet supported.

Configuration Options:
none
 */

/*
 * This file provide useful declaration like some constants
 * for configuration needed both in user's space and in kernel space
 */

/* Subdevice numbers */
#define MF624_ENC_SUBDEV 	5
#define MF624_CTR_SUBDEV 	4
#define MF624_DO_SUBDEV		3
#define MF624_DI_SUBDEV		2
#define MF624_AO_SUBDEV		1
#define MF624_AI_SUBDEV		0

/* Encoders Configuration */
#define MF624_ENC			0x0001

/* some unnecessary masks */
#define MF624_ENC_MODE			0x000100
#define MF624_ENC_COUNT			0x000101
#define MF624_ENC_RESET			0x000102
#define MF624_ENC_FILTER		0x000103

/* some opcodes */
#define MF624_ENC_MODE_4EDGE		0x00010000
#define MF624_ENC_MODE_RISING		0x00010001
#define MF624_ENC_MODE_FALLING		0x00010002
#define MF624_ENC_MODE_EITHER		0x00010003
#define MF624_ENC_COUNT_ENABLE		0x00010100
#define MF624_ENC_COUNT_DISABLE		0x00010101
#define MF624_ENC_COUNT_ENABLE_BYINPUT	0x00010102
#define MF624_ENC_COUNT_DISABLE_BYINPUT	0x00010103
#define MF624_ENC_RESET_DISABLE		0x00010200
#define MF624_ENC_RESET_ENABLE		0x00010201
#define MF624_ENC_RESET_DISABLE_BYINPUT	0x00010202
#define MF624_ENC_RESET_ENABLE_BYINPUT	0x00010203
#define MF624_ENC_RESET_ONRAISING_INPUT	0x00010204
#define MF624_ENC_RESET_ONFALLING_INPUT	0x00010205
#define MF624_ENC_RESET_ONEITHER_INPUT	0x00010206
#define MF624_ENC_FILTER_OFF		0x00010300
#define MF624_ENC_FILTER_ON		0x00010301

/* Counters Configration */
#define MF624_CTR			0x0002

/* some unnecessary masks */
#define MF624_CTR_DIRECTION		0x000200
#define MF624_CTR_REPETITION		0x000201
#define MF624_CTR_PRELOAD		0x000202
#define MF624_CTR_OUTPUT		0x000203
#define MF624_CTR_TRIGGER		0x000204
#define MF624_CTR_RETRIGGER		0x000205
#define MF624_CTR_GATE			0x000206
#define MF624_CTR_GATEPOLARITY		0x000207
#define MF624_CTR_CKSOURCE		0x000208
#define MF624_CTR_ADTRIGSRC		0x000209
#define MF624_CTR_CTR4INTSRC		0x00020A
#define MF624_CTR_CTRL			0x00020B

/* some opcodes */
#define MF624_CTR_DIRECTION_UP		0x00020000
#define MF624_CTR_DIRECTION_DOWN	0x00020001
#define MF624_CTR_REPETITION_ON		0x00020100
#define MF624_CTR_REPETITION_OFF	0x00020101
#define MF624_CTR_PRELOAD_A		0x00020200
#define MF624_CTR_PRELOAD_AB		0x00020201
#define MF624_CTR_OUTPUT_MONOSTABLE	0x00020300
#define MF624_CTR_OUTPUT_BISTABLE	0x00020301
#define MF624_CTR_OUTPUT_DIRECT		0x00020302
#define MF624_CTR_OUTPUT_INVERSE	0x00020304
#define MF624_CTR_OUTPUT_OFF		0x00020305
#define MF624_CTR_OUTPUT_ON		0x00020306
#define MF624_CTR_TRIGGER_DISABLED	0x00020400
#define MF624_CTR_TRIGGER_BYINPUT	0x00020401
#define MF624_CTR_TRIGGER_BYLOWER	0x00020402
#define MF624_CTR_TRIGGER_BYUPPER	0x00020403
#define MF624_CTR_TRIGGER_RAISING	0x00020404
#define MF624_CTR_TRIGGER_FALLING	0x00020405
#define MF624_CTR_TRIGGER_EITHER	0x00020406
#define MF624_CTR_TRIGGER_DISABLE	0x00020407
#define MF624_CTR_RETRIGGER_ON		0x00020500
#define MF624_CTR_RETRIGGER_OFF		0x00020501
#define MF624_CTR_GATE_HIGHT		0x00020600
#define MF624_CTR_GATE_BYINPUT		0x00020601
#define MF624_CTR_GATE_BYLOWER		0x00020602
#define MF624_CTR_GATE_BYUPPER		0x00020603
#define MF624_CTR_GATEPOLARITY_LOW	0x00020700
#define MF624_CTR_GATEPOLARITY_HIGH	0x00020701
#define MF624_CTR_CKSOURCE_50M		0x00020800
#define MF624_CTR_CKSOURCE_10M		0x00020801
#define MF624_CTR_CKSOURCE_1M		0x00020802
#define MF624_CTR_CKSOURCE_100K		0x00020803
#define MF624_CTR_CKSOURCE_RAISINGINPUT	0x00020804
#define MF624_CTR_CKSOURCE_FALLINGINPUT	0x00020805
#define MF624_CTR_CKSOURCE_EITHERINPUT	0x00020806
#define MF624_CTR_CKSOURCE_RAISINGLOWER	0x00020807
#define MF624_CTR_CKSOURCE_FALLINGLOWER	0x00020808
#define MF624_CTR_CKSOURCE_EITHERLOWER	0x00020809
#define MF624_CTR_CKSOURCE_RAISINGUPPER	0x0002080A
#define MF624_CTR_CKSOURCE_FALLINGUPPER	0x0002080B
#define MF624_CTR_CKSOURCE_EITHERUPPER	0x0002080C
#define MF624_CTR_ADTRIGSRC_EXT		0x00020900
#define MF624_CTR_ADTRIGSRC_INT		0x00020901
#define MF624_CTR_CTR4INTSRC_EXT	0x00020A00
#define MF624_CTR_CTR4INTSRC_INT	0x00020A01
#define MF624_CTR_CTRL_START		0x00020B00
#define MF624_CTR_CTRL_STOP		0x00020B01
#define MF624_CTR_CTRL_LOAD		0x00020B02
#define MF624_CTR_CTRL_RESET		0x00020B03
#define MF624_CTR_CTRL_TSET		0x00020B04
#define MF624_CTR_CTRL_TRESET		0x00020B05
