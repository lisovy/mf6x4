== Úvod ==
Následující text se snaží čtenáři vysvětlit (a názorně předvést) na základní úrovni kroky, které jsou ze strany operačního systému (resp. ovladače, který je jeho součástí) potřeba učinit pro zajištění správné funkce PCI zařízení/karty v počítači.

Kromě základního popisu principu bude vysvětlen konkrétní způsob implementace ovladače PCI zařízení (ve skutečnosti půjde o dva možné druhy ovladače).

Jako cílová platforma použitá v celé této práci je zvolena architektura IA-32 (také označovaná jako x86). V praxi by však mělo být možné výsledný ovladač použít i na procesorech jiných architektur. Jako operační systém byl (pro svoji kvalitu, otevřenost, všestrannost a rozšířenost) zvolen Debian GNU/Linux (verze jádra Linux 2.6.32).

Od čtenáře se pro správné pochopení textu vyžaduje základní znalost:
* funkce operačního systému (userspace vs. kernelspace, komunikace s HW)
* memory managementu (základní funkce MMU, paměťový vs. I/O prostor, memory mapped I/O)
* principu fungování obecného procesoru (privilegovaný vs. neprivilegovaný režim)

Dále také základní znalost:
* práce s operačním systémem GNU/Linux (práce v příkazové řádce, základní orientace v hierarchii souborového systému -- Filesystem Hierarchy Standard)
* práce s GNU programovacími nástroji (GCC, Make)
* porozumění kódu a programování v jazyce C

Pro výklad těchto témat bohužel není v této práci místo.  

== Humusoft MF6x4 ==
Jako PCI zařízení, pro nějž bude v této práci implementován ovladač, byla zvolena PCI karta Humusoft MF614 (http://www.humusoft.cz/produkty/datacq/old/mf614/). Jedná se o tzv. "měřící kartu". Ta sestává z několika analogových vstupů/výstupů a několika digitálních vstupů/výstupů.

Důvodem pro volbu této karty byla celkem snadná a přímočará komunikace mezi operačním systémem hostitelského systému a kartou (zjednodušeně: zapíšeme hodnotu do registru karty, ta se projeví na výstupu; pro vstup čteme průběžně měnící se hodnotu registru karty).

== PCI zařízení/karta ==
Pro komunikaci s PCI zařízením/kartou je potřeba pro něj/ni vyhradit určitý paměťový nebo I/O prostor. V případě, že jak karta tak operační systém (a MMU) budou vědět, že na určité adrese X se nachází paměťová buňka patřící PCI zařízení, může probíhat komunikace ''čtením z'' nebo ''zápisem do'' této paměťové buňky.

Nejsnazší netodou (která se u PCI nepoužívá) by bylo tyto adresy nastavit "natvrdo" v PCI zařízení a ovladači pro toto zařízení v operačním systému. Něco takového by bylo poměrně neflexibilní (místy až nepoužitelné) -- proto zařízení na PCI sběrnici využívají tzv. Plug & Play systém. Ten umožňuje dynamickou konfiguraci mapování jednotlivých PCI zařízení do paměťového a I/O prostoru.

Zjednodušeně: Ovladač v operačním systému ví přesně, kolik jak velkých paměťových a I/O úseků bude zařízení pro svoji funkci vyžadovat -- proto stačí pouze tuto paměť vyhradit pro zařízení a nějak ho o tom informovat.

Jelikož karta ze začátku nemá přidělený žádný úsek v paměťovém nebo I/O prostoru, ztěžuje to její adresaci -- to lze vyřešit pomocí tzv. "geografického" adresování, kdy je karta zvolena na základě své polohy na sběrnici. Poté, co je takto karta oslovena, jsou do jejich adresních registrů (''Base Address Registers -- BAR'') zapsány počáteční adresy již rezervovaných paměťových nebo I/O rozsahů. BAR registrů je celkem 6 (BAR0--BAR5) a leží v tzv. "konfiguračním prostoru" (configuration space) daného PCI zařízení (pozor, zcela se liší od paměťového nebo I/O prostoru). Na x86 architektuře se do něj přistupuje pomocí zapsání adresy (kam chceme v konfiguračním prostoru zapisovat) a dat (která chceme do konfiguračního prostoru zapsat) do dvou I/O portů s pevně danou adresou.

(''Pro více info: http://en.wikipedia.org/wiki/PCI_Configuration_Space'')

== UIO Ovladač ==
Takzvaný UIO ovladač je ovladačem pro PCI zařízení, který sestává ze dvou částí: malého jaderného modulu a hlavní části v user-space. Důvodem pro použití ovladače tohoto typu je, že většina jeho programového kódu běží v user-space. To je výhodné pro případy jako je tento -- kdy se snažíme (bez velkých předchozích zkušeností) vyvinout funkční ovladač. To, že ovladač poběží v uživatelském prostoru může při vývoji minimalizovat počet pádů operačního systému (ty samozřejmě nejsou nutné, ale při vývoji plnohodnotných kernel-space ovladačů mohou nastat).

= Implementační detaily =
[To be done]
 
