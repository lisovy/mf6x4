#include <linux/init.h>
#include <linux/module.h>

struct foo_t {
	int zomg;
	int lol;
	int bar;
};

static void hello_exit(void);

static int hello_init(void)
{
	struct foo_t foo;
	foo.zomg = 1;
	foo.lol = 2;
	foo.bar = 3;

	printk("Hello, world!\n");
	printk("%pR\n", &foo);
	printk("%pf\n", hello_exit);
	return 0;
}

static void hello_exit(void)
{
	printk("Goodbye, cruel world!\n");
}

module_init(hello_init);
module_exit(hello_exit);
 
MODULE_LICENSE("Dual BSD/GPL");
